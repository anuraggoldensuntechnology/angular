import { Component, OnInit } from '@angular/core';
import { LoginServiceService } from '../login-service.service'
import { Alert } from 'selenium-webdriver';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  anub: any = []
  n: any;
  constructor(private loginservice: LoginServiceService) { }


  /* =====================get================================ */
  getdetails() {
    this.loginservice.getdetails().subscribe((data) => {
      this.anub = data;
      alert(JSON.stringify(data))
    })
  }

  /*  ===================add============================= */
  add(name, type) {
    debugger
    this.loginservice.addetails(name, type).subscribe((data) => {
      debugger
      this.anub = data;
      this.getdetails()
      alert(JSON.stringify(data))
    })
  }

  /* =====================delete============================ */
  delete(id) {
    debugger
    this.loginservice.deletedetails(id).subscribe((data) => {
      this.anub = data;
      this.getdetails()

      // alert(JSON.stringify(data))
    })
  }
  name: any;
  type: any;
  updat: boolean = false;
  id:any;
  Edit(anub1) {
    this.id = anub1.id;
    this.name = anub1.name;
    this.type = anub1.type
    this.updat = true
  }
  /* ================update======================== */
  update( name,type) {
    debugger
    this.loginservice.updatedetails(name,type,this.id).subscribe((data) => {

      this.anub = data;
      this.updat = false;
      this.name='',
      this.type='',
      this.id='';
      this.getdetails;
      this.getdetails()
    })
  }
  fact(n){
    debugger
    this.factorial(n) ;
    debugger
    console.log("the factorial is",this.factorial(n));
    alert(JSON.stringify(this.factorial(n)))
    
    
     }
     
  factorial(n) {
    debugger
    if (n == 0) {
    return 1;
    } else {
    return this.factorial(n - 1) * n;
    debugger
    }
   
   
  }
  /* ======================update ends============================== */

  ngOnInit() {
    this.getdetails()
  }

}




