import { Injectable } from '@angular/core';
import { Observable, from} from 'rxjs';
import {HttpClient,HttpHeaders} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class LoginServiceService {
  delete(name: any) {
    throw new Error("Method not implemented.");
  }
  retrieveData: any;
  constructor(private http:HttpClient) { }
  httpOperations={headers:new HttpHeaders({'content-type':'application/json'})
}
// myurl='http://localhost:4300';
myurl='http://anurag1stserver.herokuapp.com';

 // apiUrl = "http://183.82.121.112:8002/get/";
  // posturl = "http://183.82.121.112:8002/";
  // imageurl = "http://183.82.121.112:8002";


  // apiUrl = "https://www.pptshopee.in/get/";
  // posturl = "https://www.pptshopee.in/";
  // imageurl = "https://www.pptshopee.in";
/* ================get==================== */
 
  getdetails():Observable<any>{
    console.log("getdetails")
    return this.http.get(this.myurl+'/medicine',{responseType:'json'})
  }


/* =================delete====================== */
deletedetails(id):Observable<any>

{
  
  console.log("deleting details")
  return this.http.post(this.myurl+'/delete',
  { 
    id:id})
  
}
/* ======================update======================== */


updatedetails(name,type,id):Observable<any>
{
  console.log("update details")
  return this.http.post(this.myurl+'/update',
  { 
    name:name,type:type,id:id})
}




/* ====================add================================ */

  addetails(name,type):Observable<any>{
  
    console.log("adding details")
    return this.http.post(this.myurl+'/add',
        {
          name:name,type:type
        })
    
    }
}

